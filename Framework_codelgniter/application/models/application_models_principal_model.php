<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal_model extends CI_Model {

	function __construct() {
        parent::__construct();
    }


    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->limit($perpage,$start);
        
        $this->db->join('morador','reserva.morador_id = morador.idMorador');
         //$this->db->join('item as imoveis', 'imoveis.item_id = tb003compromissos.item_id', "inner");
        
        //$this->db->order_by('codAgenda','desc');
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

//    function getById($id){
//        //$this->db->join('fotos','fotos.membro_id = membros.idMembro');
//        $this->db->where('idMorador',$id);
//        $this->db->limit(1);
//        return $this->db->get('morador')->row();
//    }

    function getBuscar($id, $tabela, $campoBusca) {
        $this->db->where($campoBusca, $id);
        $this->db->limit(1);
        return $this->db->get($tabela)->row();
    }
 
    function getDadosMorador($id){
        $this->db->where('idMorador',$id);
        $this->db->limit(1);
        return $this->db->get('morador')->row();
    }
    function getPreventivasRealizadas(){
        $this->db->select('*');
        $this->db->from('tbl_009_preventiva');
        $this->db->join('morador','reserva.morador_id = morador.idMorador', 'left');
        $this->db->order_by('dataReserva','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    function getVisitas(){
        $this->db->select('*');
        $this->db->from('tbl_controlevisitas');

        $this->db->join('tbl_controlevisitantes','tbl_controlevisitantes.idVisitante = tbl_controlevisitas.visitante_id','left');
        $this->db->join('tbl_imagemvisitantes','tbl_imagemvisitantes.controlevisitantes_id = tbl_controlevisitantes.idVisitante','left');
        $this->db->join('morador','morador.idMorador = tbl_controlevisitas.morador_id','left');

        $this->db->order_by('idControlevisitas','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }
    
    function getCorpoDiretivo(){
        $this->db->select('*');
        $this->db->from('corpodiretivo');
        $this->db->join('imagemdiretoria','imagemdiretoria.diretoria_id = corpodiretivo.idCorpoDiretivo','left');
        $this->db->join('morador','corpodiretivo.morador_id = morador.idMorador', 'left');
        $this->db->order_by('idCorpoDiretivo','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }
    
    function getMudancasPendentes(){
        $this->db->select('*');
        $this->db->from('mudanca');
        $this->db->join('morador','mudanca.morador_id = morador.idMorador', 'left');
        $this->db->order_by('idMudanca','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    function getMudancas(){
        $this->db->select('*');
        $this->db->from('mudanca');
        $this->db->join('morador','mudanca.morador_id = morador.idMorador', 'left');
        $this->db->order_by('idMudanca','desc');
        $this->db->limit(30);
        return $this->db->get()->result();
    }

    function getReservasPendentesPortaria(){
        $this->db->select('*');
        $this->db->from('reserva');
        $this->db->join('morador','reserva.morador_id = morador.idMorador');
        $this->db->order_by('idReserva','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    function getAtestadosVenciadosPortaria(){
        $this->db->select('*');
        $this->db->from('atestadomedico');
        $this->db->join('morador','atestadomedico.morador_id = morador.idMorador');
        $this->db->order_by('idAtestadoMedico','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    function getOcorrencias(){
        $this->db->select('*');
        $this->db->from('livro_ocorrencia');
        $this->db->join('morador','livro_ocorrencia.morador_id = morador.idMorador', 'left');
        $this->db->order_by('idOcorrencia','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    public function PCfinanceiroMesAtual($mes, $ano) {
        $query = "SELECT * FROM lancamentos WHERE month(data_vencimento) = '" . $mes . "' and year(data_vencimento) = '" . $ano . "'";
        return $this->db->query($query)->result();
    }

    public function PCNotificacaoMesAtual($mes, $ano)
    {
        $query = "SELECT * FROM multanotificacao WHERE month(data) = '". $mes ."'  and year(data) = '". $ano . "' and tipo = 'Notificacao'";
        return $this->db->query($query)->result();

    }

    public function PCAdvertenciaMesAtual($mes, $ano)
    {
        $query = "SELECT * FROM multanotificacao WHERE month(data) = '" . $mes . "'  and year(data) = '" . $ano . "' and tipo = 'Advertencia'";
        return $this->db->query($query)->result();

    }

    public function PCOcorrenciaMesAtual($mes, $ano)
    {
        $query = "SELECT * FROM (`livro_ocorrencia`) LEFT JOIN `morador` ON `livro_ocorrencia`.`morador_id` = `morador`.`idMorador` LEFT JOIN `funcionario` ON `livro_ocorrencia`.`funcionario_id` = `funcionario`.`idFuncionario` WHERE month(dataOcorrencia) ='" . $mes . "' AND year(dataOcorrencia) ='" . $ano . "' ORDER BY `idOcorrencia` desc" ;
        return $this->db->query($query)->result();
    }

    public function PCMultaMesAtual($mes, $ano)
    {
        $query = "SELECT * FROM multanotificacao WHERE month(data) = '" . $mes . "'  and year(data) = '" . $ano . "' and tipo = 'Multa'";
        return $this->db->query($query)->result();

    }

    /////// aqui

    public function PCMfinanceiroMesAtual() {
        $query = "SELECT * FROM lancamentos WHERE month(data_vencimento) = MONTH(now()) and year(data_vencimento) = YEAR(NOW()) ";
        return $this->db->query($query)->result();
    }

    public function PCMNotificacaoMesAtual()
    {
        $query = "SELECT * FROM multanotificacao WHERE month(data) = MONTH(now())  and year(data) = YEAR(now()) and tipo = 'Notificacao'";
        return $this->db->query($query)->result();

    }

    public function PCMAdvertenciaMesAtual()
    {
        $query = "SELECT * FROM multanotificacao WHERE month(data) = MONTH(now())  and year(data) = year(now()) and tipo = 'Advertencia'";
        return $this->db->query($query)->result();

    }

    public function PCMOcorrenciaMesAtual()
    {
        $query = "SELECT * FROM (`livro_ocorrencia`)  WHERE month(dataOcorrencia) =MONTH(now()) AND year(dataOcorrencia) =year(now()) ORDER BY `idOcorrencia` desc" ;
        return $this->db->query($query)->result();
    }

    public function PCMMultaMesAtual()
    {
        $query = "SELECT * FROM multanotificacao WHERE month(data) = MONTH(now())  and year(data) = YEAR(now()) and tipo = 'Multa'";
        return $this->db->query($query)->result();

    }

    function getMenusDocumentos(){
        $this->db->select('*');
        $this->db->from('tbl_menudoccond');
        $this->db->join('tbl_doccondominio','tbl_doccondominio.menudoccond_id = tbl_menudoccond.idMenuDocCond', 'left');
        $this->db->order_by('idMenuDocCond','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }
    function getReunioes(){
        $this->db->select('*');
        $this->db->from('reuniao');
        $this->db->join('atasreuniao','atasreuniao.reuniao_id = reuniao.idReuniao','left');
        $this->db->order_by('idReuniao','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }
    function getNotificacoes(){
        $this->db->select('*');
        $this->db->from('multanotificacao');
         $this->db->join('morador','multanotificacao.morador_id = morador.idMorador', 'left');
         $this->db->order_by('idMultanotificacao','desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    function getClassificado($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        
        $this->db->join('imagemclassificados','imagemclassificados.classificados_id = classificados.cd_clas', 'left');
         //$this->db->join('item as imoveis', 'imoveis.item_id = tb003compromissos.item_id', "inner");
        
        $this->db->order_by('cd_clas','desc');
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

    function getItemClassificado(){
        $this->db->select('*');
        $this->db->from('classificados');
        $this->db->order_by('cd_clas','desc');
        $this->db->join('imagemclassificados','imagemclassificados.classificados_id = classificados.cd_clas', 'left');
        return $this->db->get()->result();
    }

    function getClassficados(){
        $this->db->select('*');
        $this->db->from('classificados');
        $this->db->order_by('cd_clas','desc');
        $this->db->join('imagemclassificados','imagemclassificados.classificados_id = classificados.cd_clas', 'left');
        return $this->db->get()->result();
    }

    function getFuncionarios($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->limit($perpage,$start);
        
        $this->db->join('imagemfuncionario','imagemfuncionario.funcionario_id = funcionario.idFuncionario', 'left');
         //$this->db->join('item as imoveis', 'imoveis.item_id = tb003compromissos.item_id', "inner");
        
        $this->db->order_by('idFuncionario','desc');
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }

     function getByIdItemClassificado($id){
        $this->db->join('imagemclassificados','imagemclassificados.classificados_id = classificados.cd_clas','left');
        $this->db->join('morador','morador.idMorador = classificados.morador_id','left');
        $this->db->where('cd_clas',$id);
        $this->db->limit(1);
        return $this->db->get('classificados')->row();
    }
    
    public function buscarNoticiasSite(){
        $this->db->select('*');
        $this->db->from('noticia');
         $this->db->join('imagensnoticia','imagensnoticia.noticia_id = noticia.idNoticia');
         $this->db->order_by('idNoticia','desc');
         $this->db->limit(10);
        return $this->db->get()->result();
    }

    function getQtdContas(){
        $sql = "SELECT count(*) as qtd FROM `lancamentos` WHERE CURDATE() > data_vencimento AND (tipo ='despesa' OR tipo ='receita') AND baixado = 0";
        return $this->db->query($sql)->result();
    }

    function getQtdAtestadoMedico(){
        $sql = "SELECT count(*) as qtd FROM `atestadomedico` WHERE CURDATE() > dataVencimento";
        return $this->db->query($sql)->result();
    }

    function getReservas() {
        $this->db->select('dataReserva, itemReservado, morador.nr_apartamento, morador.bloco_id');
        $this->db->from('reserva');
        $this->db->join('morador','reserva.morador_id = morador.idMorador');
        return $this->db->get()->result();
    }

    function getMoradores() {
        $this->db->select('idMorador, cpfMorador, nm_morador, sobreNomeMorador, nr_apartamento, bloco_id, relacaoImovel,email_morador, nr_celular');
        $this->db->from('morador');
        return $this->db->get()->result();
    }

    function count($table){
       return $this->db->count_all($table);
    }

    public function emailMorador($idMorador) {
        $a = $this->db->query("SELECT email_morador FROM morador WHERE idMorador = $idMorador ")->row()->email_morador;
        return $a;
    }

    public function getNotificacaoPorBlocos(){
        $this->db->select('bloco_id, count(bloco_id) AS num_of_time');
        $this->db->from('multanotificacao');
        $this->db->join('morador','multanotificacao.morador_id = morador.idMorador', "left");
        $this->db->where("month(data) = month('".strftime('%Y-%m-%d')."')");
        $this->db->where("year(data) = year('".strftime('%Y-%m-%d')."')");
        $this->db->group_by('bloco_id');
        return $this->db->get()->result();

    }

    public function getOcorrenciaPorBlocos(){
        $this->db->select('bloco_id, count(bloco_id) AS num_of_time');
        $this->db->from('livro_ocorrencia');
        $this->db->join('morador','livro_ocorrencia.morador_id = morador.idMorador', "left");
        $this->db->where("month(dataOcorrencia) = month('".strftime('%Y-%m-%d')."')");
        $this->db->where("year(dataOcorrencia) = year('".strftime('%Y-%m-%d')."')");
        $this->db->group_by('bloco_id');
        return $this->db->get()->result();

    }

    public function getMoradoresMaisNotificados() {
        $this->db->select('nm_morador,bloco_id,nr_apartamento, count(idMorador) AS maisNotificados');
        $this->db->from('multanotificacao');
        $this->db->join('morador', 'multanotificacao.morador_id = morador.idMorador', "left");
        $this->db->group_by('bloco_id');
        $this->db->group_by('nr_apartamento');
        $this->db->order_by('maisNotificados', 'desc');
        $this->db->limit(6);
        return $this->db->get()->result();

    }

    public function getMoradoresMaisOcorrencias() {
        $this->db->select('nm_morador,bloco_id,nr_apartamento, count(idMorador) AS maisOcorrencias');
        $this->db->from('livro_ocorrencia');
        $this->db->join('morador', 'livro_ocorrencia.morador_id = morador.idMorador', "left");
        $this->db->group_by('bloco_id');
        $this->db->group_by('nr_apartamento');
        $this->db->order_by('maisOcorrencias', 'desc');
        $this->db->limit(5);
        return $this->db->get()->result();

    }

    public function getOcorrenciasMaior() {
        $this->db->select('categoria, count(categoria) AS ocorrenciaMaior');
        $this->db->from('livro_ocorrencia');
        $this->db->where("month(dataOcorrencia) = month('".strftime('%Y-%m-%d')."')");
        $this->db->where("year(dataOcorrencia) = year('".strftime('%Y-%m-%d')."')");
        $this->db->group_by('categoria');
        $this->db->order_by('ocorrenciaMaior', 'desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    public function getNotificacaoMaior() {
        $this->db->select('categoria, count(categoria) AS notificacaoMaior');
        $this->db->from('multanotificacao');
        $this->db->where("month(data) = month('".strftime('%Y-%m-%d')."')");
        $this->db->where("year(data) = year('".strftime('%Y-%m-%d')."')");
        $this->db->group_by('categoria');
        $this->db->order_by('notificacaoMaior', 'desc');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    public function getNotificacaoPorTipo() {
        $this->db->select('tipo, count(tipo) AS numeroPortipo');
        $this->db->from('multanotificacao');
        $this->db->where("month(data) = month('".strftime('%Y-%m-%d')."')");
        $this->db->where("year(data) = year('".strftime('%Y-%m-%d')."')");
        $this->db->group_by('tipo');
        $this->db->order_by('numeroPortipo', 'desc');
        return $this->db->get()->result();
    }

}

/* End of file vendas_model.php */
/* Location: ./application/models/vendas_model.php */
