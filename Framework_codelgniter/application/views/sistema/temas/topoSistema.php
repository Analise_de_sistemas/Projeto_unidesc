<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Highdmin - Responsive Bootstrap 4 Admin Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
	<meta content="Coderthemes" name="author" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<!-- App favicon -->
	<link rel="shortcut icon" href="assets/images/favicon.ico">

<!--    load css-->
	<?php $this->load->helper("csssistema_helper"); ?>

<!--    load javascript-->
    <?php $this->load->helper("jssistematop_helper"); ?>

</head>

<body>

    <div id="wrapper">

        <?php $this->load->view("sistema/temas/menulateral");?>

        <?php $this->load->view("sistema/temas/contetpage"); ?>

    </div>

<!--    java script de baixo-->
    <?php $this->load->helper("jssistemadown_helper");  ?>
</body>
</html>
